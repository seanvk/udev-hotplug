/* cc -g -Wall -W -I/usr/include/libudev     udev-hotplug.c  -ludev  -o udev-hotplug */

#include <libudev.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>

int
main (void)
{
  struct udev *udev;
  struct udev_enumerate *enumerate;
  struct udev_list_entry *devices, *dev_list_entry;
  struct udev_device *dev;

  struct udev_monitor *uevent_monitor;
  int fd;
  int ret;

  /* udev object */
  udev = udev_new ();
  if (!udev)
  {
    fprintf (stderr, "failed to create udev object\n");
    goto out;
  }
  /* uevent monitor */
  uevent_monitor = udev_monitor_new_from_netlink (udev, "udev");
  if (!uevent_monitor)
  {
    fprintf (stderr, "failed to create udev event monitor\n");
    goto out;
  }
  /* set filter for major/minor */
  ret = udev_monitor_filter_add_match_subsystem_devtype (uevent_monitor,
                     "drm", "drm_minor");
  if (ret < 0)
  {
    fprintf (stderr, "failed to filter for drm events\n");
    goto out;
  }
  /* enable filter */
  ret = udev_monitor_enable_receiving (uevent_monitor);
  if (ret < 0)
  {
    fprintf (stderr, "failed to enable udev event reception\n");
    goto out;
  }
  /* get file descriptor */
  fd = udev_monitor_get_fd (uevent_monitor);
  if (!fd)
    goto out;

  /* 'drm' subsystem list of devices */
  enumerate = udev_enumerate_new (udev);
  udev_enumerate_add_match_subsystem (enumerate, "drm");
  udev_enumerate_scan_devices (enumerate);
  devices = udev_enumerate_get_list_entry (enumerate);

  /* For each item enumerated, print out its information. */
  udev_list_entry_foreach (dev_list_entry, devices)
  {
    const char *path;
    const char *devnode;

    /* report device nodes and supported connectors */
    path = udev_list_entry_get_name (dev_list_entry);
    dev = udev_device_new_from_syspath (udev, path);
    devnode = udev_device_get_devnode (dev);
    if (devnode != NULL)
      printf ("Device Node Path: %s\n", devnode);
    else
      printf ("Device connectors: %s\n", path);

    udev_device_unref (dev);
  }
  /* Free the enumerator object */
  udev_enumerate_unref (enumerate);

  /* Poll uevents */
  while (1)
  {
    /* non-blocking select of events */
    fd_set fds;
    struct timeval tv;
    int ret;
    const char *hotplug;

    FD_ZERO (&fds);
    FD_SET (fd, &fds);
    tv.tv_sec = 0;
    tv.tv_usec = 0;

    ret = select (fd + 1, &fds, NULL, NULL, &tv);

    /* Assure the file descriptor has received data. */
    if (ret > 0 && FD_ISSET (fd, &fds))
    {
      printf ("\nselect() says there should be data\n");

      /* non-blocking reception of device */
      dev = udev_monitor_receive_device (uevent_monitor);
      if (dev)
      {
        hotplug = udev_device_get_property_value (dev, "HOTPLUG");

        printf ("Got Device\n");
        printf ("   Node: %s\n", udev_device_get_devnode (dev));
        printf ("   Subsystem: %s\n", udev_device_get_subsystem (dev));
        printf ("   Devtype: %s\n", udev_device_get_devtype (dev));

        printf ("   Action: %s\n", udev_device_get_action (dev));
        printf ("   Hotplug: %s\n", hotplug);
        udev_device_unref (dev);
      }
      else
      {
        printf ("No Device from receive_device(). An error occured.\n");
      }
    }
    usleep (250 * 1000);
    printf (".");
    fflush (stdout);
  }

out:
  if (uevent_monitor)
    udev_monitor_unref (uevent_monitor);
  if (udev)
    udev_unref (udev);

  return 0;
}
