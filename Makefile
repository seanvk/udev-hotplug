all: udev-hotplug

#CFLAGS += -O2 -Wall -W -I/usr/include/libdrm
CFLAGS += -g -Wall -W -I/usr/include/libudev
LDLIBS += -ludev

OBJS := udev-hotplug.o

climit: $(OBJS)
	gcc -o udev-hotplug $(OBJS)



%.o: %.c Makefile
	@echo "  CC  $<"
	@[ -x /usr/bin/cppcheck ] && /usr/bin/cppcheck -q $< || :
	@$(CC) $(CFLAGS) $(LDLIBS) -c -o $@ $<


clean:
	rm  -f *~ udev-hotplug *.o

